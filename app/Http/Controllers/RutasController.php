<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{
    public function index(){
        return view('portal.index');
    }



    // RUTAS USUARIOS

    public function usuarios(){
        return view('usuarios.index');
    }

    public function usuarios_register(){
        return view('usuarios.list_register');
    }

    public function usuarios_tracking(){
        return view('usuarios.list_tracking');
    }



    // RUTAS FACTURAS
    
    public function facturas(){
        return view('facturas.index');
    }

    public function facturas_ocr(){
        return view('facturas.list_ocr');
    }

    public function facturas_submit(){
        return view('facturas.list_enviadas');
    }

    public function facturas_slope(){
        return view('facturas.list_pendientes');
    }



    // RUTAS FACTURAS 

    public function consumos(){
        return view('consumos.index');
    }
    
    public function tarifarios(){
        return view('tarifarios.index');
    }

    public function reportes(){
        return view('reportes.index');
    }
}
