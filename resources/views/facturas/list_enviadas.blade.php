@extends('layouts.app')
@section('content')

<div class="row">
  <div class="col p-3">
      <h3>Listado de facturas enviadas</h3>
  </div>
</div>


<table class="table mx-auto" id="example">
    <thead>
      <tr>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
      </tr>
      <tr>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
      </tr>
    </tbody>
  </table>

@endsection