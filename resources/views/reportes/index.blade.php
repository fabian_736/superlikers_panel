@extends('layouts.app')
@section('content')


    <div class="row">
      <div class="col-lg-12 position-relative z-index-2">
        <div class="card card-plain mb-4">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-lg-6">
                <div class="d-flex flex-column h-100">
                  <h2 class="font-weight-bolder mb-0">Reportes</h2>
                </div>
              </div>
            </div>
            <div class="row my-3">
                <div class="col-lg-6">
                  <div class="d-flex flex-column h-100">
                    <h4 class="font-weight-bolder mb-0">Filtros</h4>
                  </div>
                </div>
              </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                    <div class="col-3">
                        <div class="row-reverse">
                            <label for="">Facturas</label>
                            <select name="" id="" class="form-control" style="cursor: pointer">
                                <option value="" disabled selected>Escoge una opción...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row-reverse">
                            <label for="">Consumos</label>
                            <select name="" id="" class="form-control" style="cursor: pointer">
                                <option value="" disabled selected>Escoge una opción...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row-reverse">
                            <label for="">Tarifario</label>
                            <select name="" id="" class="form-control" style="cursor: pointer">
                                <option value="" disabled selected>Escoge una opción...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row-reverse">
                            <label for="">Fecha</label>
                            <select name="" id="" class="form-control" style="cursor: pointer">
                                <option value="" disabled selected>Escoge una opción...</option>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    

    <div class="row mx-auto p-3">
        <div class="col-lg-6">
            <div id="chart">
            </div>
        </div>
        <div class="col-lg-6">
            <div id="chart2">
            </div>
        </div>
    </div>

    <div class="row mx-auto p-3">
        <div class="col-lg-6">
            <div id="chart3">
            </div>
        </div>
        <div class="col-lg-6">
            <div id="chart4">
            </div>
        </div>
    </div>

    <div class="row mx-auto p-3">
        <div class="col-lg-6 d-flex justify-content-center">
            <div id="chart5">
            </div>
        </div>
        <div class="col-lg-6">
            <div id="chart6">
            </div>
        </div>
    </div>



    <script>

        //Chart 1
        var options = {
    chart: {
      height: 380,
      width: "100%",
      type: "line"
    },
    series: [
      {
        name: "Series 1",
        data: [45, 52, 38, 45, 19, 33]
      }
    ],
    xaxis: {
      categories: [
        "01 Jan",
        "02 Jan",
        "03 Jan",
        "04 Jan",
        "05 Jan",
        "06 Jan"
      ]
    }
  };
  
  var chart = new ApexCharts(document.querySelector("#chart"), options);
  
  chart.render();



  //Chart 2


  var options2 = {
          series: [{
          name: 'series1',
          data: [31, 40, 28, 51, 42, 109, 100]
        }, {
          name: 'series2',
          data: [11, 32, 45, 32, 34, 52, 41]
        }],
          chart: {
          height: 350,
          type: 'area'
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth'
        },
        xaxis: {
          type: 'datetime',
          categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
          x: {
            format: 'dd/MM/yy HH:mm'
          },
        },
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
        chart2.render();
      
      

    // Chart 3

    var options3 = {
          series: [{
          data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan',
            'United States', 'China', 'Germany'
          ],
        }
        };


        var chart3 = new ApexCharts(document.querySelector("#chart3"), options3);
        chart3.render();



        //Chart 4

        var options4 = {
          series: [{
          name: 'Marine Sprite',
          data: [44, 55, 41, 37, 22, 43, 21]
        }, {
          name: 'Striking Calf',
          data: [53, 32, 33, 52, 13, 43, 32]
        }, {
          name: 'Tank Picture',
          data: [12, 17, 11, 9, 15, 11, 20]
        }, {
          name: 'Bucket Slope',
          data: [9, 7, 5, 8, 6, 9, 4]
        }, {
          name: 'Reborn Kid',
          data: [25, 12, 19, 32, 25, 24, 10]
        }],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: true,
          },
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        title: {
          text: 'Fiction Books Sales'
        },
        xaxis: {
          categories: [2008, 2009, 2010, 2011, 2012, 2013, 2014],
          labels: {
            formatter: function (val) {
              return val + "K"
            }
          }
        },
        yaxis: {
          title: {
            text: undefined
          },
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + "K"
            }
          }
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'top',
          horizontalAlign: 'left',
          offsetX: 40
        }
        };

        var chart4 = new ApexCharts(document.querySelector("#chart4"), options4);
        chart4.render();


        // Chart 5
        var options5 = {
          series: [44, 55, 13, 43, 22],
          chart: {
          width: 580,
          type: 'pie',
        },
        labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart5 = new ApexCharts(document.querySelector("#chart5"), options5);
        chart5.render();


// Chart 6
var options6 = {
          series: [{
          name: 'Series 1',
          data: [80, 50, 30, 40, 100, 20],
        }],
          chart: {
          height: 550,
          type: 'radar',
        },
        title: {
          text: 'Basic Radar Chart'
        },
        xaxis: {
          categories: ['January', 'February', 'March', 'April', 'May', 'June']
        }
        };

        var chart6 = new ApexCharts(document.querySelector("#chart6"), options6);
        chart6.render();
      
  
    </script>

  @endsection


  