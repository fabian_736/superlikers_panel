<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('portal',[RutasController::class,'index'])->name('portal.index');
Route::get('consumos',[RutasController::class,'consumos'])->name('consumos.index');

// RUTAS USUARIOS
Route::get('usuarios',[RutasController::class,'usuarios'])->name('usuarios.index');
Route::get('usuarios/list/register',[RutasController::class,'usuarios_register'])->name('usuarios.list_register');
Route::get('usuarios/list/tracking',[RutasController::class,'usuarios_tracking'])->name('usuarios.list_tracking');

// RUTAS FACTURAS
Route::get('facturas',[RutasController::class,'facturas'])->name('facturas.index');
Route::get('facturas/list/ocr',[RutasController::class,'facturas_ocr'])->name('facturas.ocr');
Route::get('facturas/list/submit',[RutasController::class,'facturas_submit'])->name('facturas.submit');
Route::get('facturas/list/slope',[RutasController::class,'facturas_slope'])->name('facturas.slope');


Route::get('tarifarios',[RutasController::class,'tarifarios'])->name('tarifarios.index');

Route::get('reportes',[RutasController::class,'reportes'])->name('reportes.index');